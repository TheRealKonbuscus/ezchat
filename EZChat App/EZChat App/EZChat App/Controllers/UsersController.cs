﻿using EZChat_App.App_Start;
using EZChat_App.Models;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EZChat_App.Controllers
{
    public class UsersController : Controller
    {
        MongoContext _dbContext;
        // GET: Modifier profile
        public ActionResult Index()
        {
            _dbContext = new MongoContext();
            var user = Session["User"];
            ViewData["CountriesList"] = _dbContext.database.GetCollection<Countries>("Countries").FindAll().ToList();
            ViewData["SpokenLang"] = _dbContext.database.GetCollection<Lang>("Lang").FindAll().ToList();
            return View(user);
        }

        public ActionResult SearchFriends()
        {
            return View();
        }

        /// <summary>
        /// Mise à jour de l'utilisateur
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public JsonResult UpdateUser(Users user)
        {
            _dbContext = new MongoContext();
            Users UserToEdit = Session["User"] as Users;
            try
            {
                _dbContext.database.GetCollection<Users>("users").FindAndModify(
                                Query.EQ("_id", UserToEdit._id),
                                null,
                                Update.Set("UserName", user.UserName)
                                .Set("FirstName", user.FirstName)
                                .Set("LastName", user.LastName)
                                .Set("CountryCode", user.CountryCode)
                                .Set("LanguageCode", user.LanguageCode)
                                .Set("PictureProfile", user.PictureProfile == null ? "" : user.PictureProfile)
                                .Set("Email", user.Email)
                                .Set("BirthDate", user.BirthDate),
                                true);

                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch(Exception e)
            {
                e.GetBaseException();
                return Json(false, JsonRequestBehavior.AllowGet);
            }

        }
    }
}